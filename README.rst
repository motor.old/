Introduction
============
Motor is the top-level management project to Servo, Checkin and Docs.

- manage.servoapp.com (only super-user access)
- docs.servoapp.com (documentation)
- checkin.servoapp.com (online service check-in)
- signin.servoapp.com (global login)
- signup.servoapp.com (dish out fresh Servo installs)

Servo (apps/core)
=================
- Redirect logins to correct app URL based on email (signin.servoapp.com)
- Creating Servo instances
- Updating Servo instances
- Manage scheduled tasks


Checkin (apps/checkin)
======================
- checkin.servoapp.com
- Create and manage SP accounts and primary admins


Docs (apps/docs)
================
- docs.servoapp.com
- Provide Servo documentation
- Manage articles
- Manage attachments
