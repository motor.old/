from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from apps.core.views import *
from apps.issues.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    #url(r'^$', 'docs.views.index', name='docs-index'),
    url(r'^$', welcome, name='issues-welcome'),
    url(r'^issues/thanks/$', thanks, name='issues-thanks'),
    url(r'^issues/notes/$', details, name='issues-details'),
    url(r'^issues/notes/([a-z]+)/$', details, name='issues-details'),
    url(r'^issues/(\d+)?/$', list_issues, name='issues-index'),
    url(r'^issues/devices/$', choose_device, name='issues-choose_device'),
    url(r'^issues/devices/([a-z]+)/$', choose_device, name='issues-choose_device'),
    url(r'^issues/warranty/$', warranty, name='issues-warranty'),
    url(r'^issues/customer/$', customer, name='issues-customer'),
    url(r'^manage/$', LoginView.as_view(), name='core-login'),
    url(r'^manage/logout/$', logout, name='core-logout'),
    url(r'^manage/docs/$', ArticleListView.as_view(), name='core-list_docs'),
    url(r'^manage/docs/add/$', ArticleCreateView.as_view(), name='core-add_article'),
    url(r'^manage/docs/(?P<pk>\d+)/$', ArticleEditView.as_view(), name='core-edit_article'),
    url(r'^manage/users/$', UserListView.as_view(), name='core-list_users'),
    url(r'^manage/users/(?P<pk>\d+)/$', UserEditView.as_view(), name='core-edit_user'),
    url(r'^manage/users/add/$', UserCreateView.as_view(), name='core-add_user'),
    url(r'^manage/providers/$', ProvidersListView.as_view(), name='core-list_sites'),
    url(r'^manage/providers/(?P<pk>\d+)/$', ProviderEditView.as_view(), name='core-edit_provider'),
    url(r'^manage/providers/add/$', CreateSiteView.as_view(), name='core-add_site'),
    url(r'^manage/issues/$', IssueListView.as_view(), name='issues-list_issues'),
    url(r'^manage/issues/add/$', IssueCreateView.as_view(), name='issues-add_issue'),
    url(r'^manage/issues/(?P<pk>\d+)/$', IssueEditView.as_view(), name='issues-edit_issue'),
    url(r'^terms/$', TemplateView.as_view(template_name="terms.html"),
        {'title': 'ServoApp Terms of Service'}),
    (r'^admin/', include(admin.site.urls)),
   #url(r'^([\w\-]+)/$', 'docs.views.view_article', name='docs-view'),
)
