from django import forms
from django.contrib import auth
from apps.core.models import ServiceProvider, User
from django.forms.extras.widgets import SelectDateWidget


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        user = auth.authenticate(
            username=cleaned_data['username'],
            password=cleaned_data['password']
        )
        if user is not None and user.is_active:
            cleaned_data['user'] = user
        else:
            raise forms.ValidationError('Incorrect username or password')

        return cleaned_data


class EditProviderForm(forms.ModelForm):
    class Meta:
        model = ServiceProvider
        widgets = {
            'date_started': SelectDateWidget(),
            'date_ends': SelectDateWidget()
        }


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('groups', 'user_permissions')
