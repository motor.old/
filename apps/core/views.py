from django.contrib import auth
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, CreateView, UpdateView
from apps.core.forms import LoginForm, EditProviderForm, UserEditForm
from django.utils.translation import ugettext as _

from apps.core.models import ServiceProvider, User
from apps.docs.models import Article

class DefaultListView(ListView):
    template_name = 'core/list.html'


class DefaultEditView(UpdateView):
    template_name = 'core/form.html'
    def get_context_data(self, **kwargs):
        context = super(DefaultEditView, self).get_context_data(**kwargs)
        context['object_list'] = self.model.objects.all()
        return context


class CreateSiteView(CreateView):
    model = ServiceProvider
    form_class = EditProviderForm
    template_name = 'core/form.html'
    success_url = '/manage/sites/'


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'login.html'
    success_url = '/manage/providers/'

    def form_valid(self, form):
        auth.login(self.request, form.cleaned_data['user'])
        return super(LoginView, self).form_valid(form)


class ProvidersListView(DefaultListView):
    model = ServiceProvider


class ProviderEditView(DefaultEditView):
    model = ServiceProvider


class UserListView(DefaultListView):
    model = User


class UserEditView(DefaultEditView):
    model = User
    form_class = UserEditForm


class UserCreateView(CreateView):
    model = User
    form_class = UserEditForm
    template_name = 'core/form.html'


class ArticleListView(DefaultListView):
    model = Article


class ArticleEditView(DefaultEditView):
    model = Article


class ArticleCreateView(CreateView):
    model = Article
    template_name = 'core/form.html'


def logout(request):
    auth.logout(request)
    messages.success(request, _('You have signed out'))
    return redirect('/manage/')
