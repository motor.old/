from django.contrib import admin
from apps.core.models import ServiceProvider

admin.site.register(ServiceProvider)
