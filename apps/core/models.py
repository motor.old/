import uuid
import gsxws
from django.db import models
from django.contrib.contenttypes import generic
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext as _

from django.template.defaultfilters import slugify


class User(AbstractUser):
    sp = models.ForeignKey('ServiceProvider', null=True)

    def __unicode__(self):
        if self.first_name and self.last_name:
            return self.first_name + ' ' + self.last_name
        return self.username

    def get_absolute_url(self):
        return '/manage/users/%d/' % self.pk


class ServiceProvider(models.Model):
    name = models.CharField(
        unique=True,
        max_length=128,
        default='Apple Service Provider'
    )
    uuid = models.CharField(
        unique=True,
        max_length=36,
        editable=False,
        default=lambda: str(uuid.uuid4())
    )
    BACKEND_CHOICES = (
        ('http', 'HTTP'),
        ('smtp', 'SMTP'),
        ('servo', 'Servo')
    )
    backend_type = models.CharField(
        max_length=6,
        default='servo',
        choices=BACKEND_CHOICES
    )

    http_url = models.URLField(null=True, blank=True)
    http_username = models.CharField(max_length=128, null=True, blank=True)
    http_password = models.CharField(max_length=128, null=True, blank=True)
    smtp_address = models.EmailField(null=True, blank=True)
    servo_url = models.URLField(null=True, blank=True)
    install_id = models.CharField(max_length=2)

    is_active = models.BooleanField(default=True)
    main_user = models.ForeignKey(User, null=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return '/manage/providers/%d/' % self.pk


class Subscription(models.Model):
    sp = models.ForeignKey(ServiceProvider)
    start_date = models.DateField()
    end_date = models.DateField()
    billing_period = models.PositiveIntegerField(default=12)


class TaggedItem(models.Model):
    "A generic tagged item"
    tag = models.CharField(max_length=128)
    slug = models.SlugField()
    kind = models.CharField(max_length=64, default='')

    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType)
    content_object = generic.GenericForeignKey("content_type", "object_id")

    def save(self, *args, **kwargs):
        self.slug = slugify(self.tag)
        super(TaggedItem, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.tag

    class Meta:
        unique_together = ("content_type", "object_id", "tag",)


class GsxAccount(models.Model):
    sp = models.OneToOneField(ServiceProvider)
    sold_to = models.CharField(max_length=10)
    user_id = models.CharField(max_length=128)
    password = models.CharField(max_length=256)
    region = models.CharField(
        max_length=3,
        choices=gsxws.GSX_REGIONS,
        verbose_name=_("Region")
    )
    environment = models.CharField(
        max_length=2,
        verbose_name=_("Environment"),
        choices=gsxws.ENVIRONMENTS,
        default=gsxws.ENVIRONMENTS[0][0]
    )
