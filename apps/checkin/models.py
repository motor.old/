import uuid
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _

from apps.core.models import TaggedItem, ServiceProvider


class Device(models.Model):
    sn = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("Serial Number")
    )
    description = models.CharField(
        max_length=128,
        default=_("New Device"),
        verbose_name=_("description")
    )
    WARRANTY_CHOICES = (
        ('ALW', _("Apple Limited Warranty")),
        ('APP', _("AppleCare Protection Plan")),
        ('CSC', _("Customer Satisfaction (CS) Code")),
        ('CBC', _("Custom Bid Contracts")),
        ('WTY', _("3'rd Party Warranty")),
        ('OOW', _("Out Of Warranty (No Coverage)")),
        ('NA', _("Unknown")),
    )

    warranty_status = models.CharField(
        max_length=3,
        default="NA",
        choices=WARRANTY_CHOICES,
        verbose_name=_("Warranty Status")
    )
    username = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("username")
    )
    password = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("password")
    )


class Customer(models.Model):
    sp = models.ForeignKey(ServiceProvider)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    street_address = models.CharField(
        null=True,
        blank=True,
        max_length=128,
        verbose_name=_('address')
    )
    zip_code = models.CharField(
        null=True,
        blank=True,
        max_length=32,
        verbose_name=_('ZIP Code')
    )
    city = models.CharField(
        null=True,
        blank=True,
        max_length=32,
        verbose_name=_('city')
    )
    devices = models.ManyToManyField(Device, null=True)


class ServiceOrder(models.Model):
    sp = models.ForeignKey(ServiceProvider)
    uuid = models.CharField(
        max_length=36,
        unique=True,
        default=lambda: str(uuid.uuid4())
    )
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    notes = models.TextField(default='')
    customer = models.ForeignKey(Customer)
    devices = models.ManyToManyField(Device, null=True)
    tags = models.ManyToManyField(TaggedItem)

    servo_id = models.CharField(max_length=7, default='', editable=False)
    status = models.TextField(default='', blank=True)

    def add_accessory(self, accessory):
        tag = TaggedItem()
        tag.kind = 'accessory'
        tag.save()

    def submit(self):
        pass
