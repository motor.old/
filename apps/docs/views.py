# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect

from docs.models import Article


def index(request):
    data = {'topics': Article.objects.filter(published=True)}
    data['title'] = u'ServoApp Help'
    return render(request, "docs_index.html", data)


def view_article(request, slug):
    article = Article.objects.get(slug=slug)
    data = {'article': article}
    data['topics'] = Article.objects.filter(published=True)
    data['title'] = article.title
    return render(request, 'docs_view.html', data)
