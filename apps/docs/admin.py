from django.contrib import admin
from apps.docs.models import Article, Image

admin.site.register(Article)
admin.site.register(Image)
