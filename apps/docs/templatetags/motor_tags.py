# -*- coding: utf-8 -*-

from django import template
register = template.Library()

from django.utils import safestring


@register.filter
def markdown(text):
    import markdown
    result = markdown.markdown(text)
    return safestring.mark_safe(result)
