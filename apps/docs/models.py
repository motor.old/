from django.db import models
from django.template.defaultfilters import slugify


class Image(models.Model):
    image = models.ImageField(upload_to='media/images')
    title = models.CharField(max_length=128, null=True)


class Article(models.Model):
    slug = models.SlugField(editable=False)
    title = models.CharField(max_length=255, default='New Article')
    content = models.TextField(default='')
    published = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    images = models.ManyToManyField(Image, null=True, editable=False)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/manage/docs/%d/' % self.pk

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)
