from django.db import models
from django.utils.translation import ugettext as _

from apps.core.models import ServiceProvider
from apps.checkin.models import ServiceOrder


class Issue(models.Model):
    sp = models.ForeignKey(ServiceProvider)
    keywords = models.TextField(default='')
    description = models.CharField(max_length=256, default=_('No power'))

    def __unicode__(self):
        return self.description

    def get_absolute_url(self):
        return '/manage/issues/%d/' % self.pk


class Question(models.Model):
    issue = models.ForeignKey(Issue)
    question = models.CharField(max_length=256)
    description = models.TextField(default='')
    required = models.BooleanField(default=True)

    def next(self):
        return Question.objects.get(issue=self.issue, pk__gt=self.pk)

    def prev(self):
        return Question.objects.get(issue=self.issue, pk__lt=self.pk)

    def __unicode__(self):
        return self.question


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice = models.CharField(max_length=256)

    def __unicode__(self):
        return self.choice


class Answer(models.Model):
    choice = models.ForeignKey(Choice)
    answer = models.CharField(max_length=256)
    so = models.ForeignKey(ServiceOrder)
