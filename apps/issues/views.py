from django import forms
from gsxws import products
from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.utils.translation import ugettext as _
from django.views.generic.edit import FormView, CreateView, UpdateView
from apps.core.views import DefaultEditView, DefaultListView
from apps.issues.models import Issue, Question, Choice


class WarrantyForm(forms.Form):
    sn = forms.CharField(
        required=False,
        initial='DGKFL06JDHJP',
        label=_('Serial number')
    )


class IssueForm(forms.Form):
    description = forms.CharField(
        required=False,
        widget=forms.Textarea()
    )
    attachment = forms.FileField(required=False)


class CustomerForm(forms.Form):
    fname = forms.CharField(label=_('First name'), initial='Filipp')
    lname = forms.CharField(label=_('Last name'), initial='Lepalaan')
    email = forms.EmailField(label=_('Email'), initial='filipp@fps.ee')
    phone = forms.CharField(label=_('Phone number'), initial='451202717')
    address = forms.CharField(label=_('Address'), initial='Kustaankatu 2 C 96')
    city = forms.CharField(label=_('City'), initial='Helsinki')
    postal_code = forms.CharField(label=_('Postal code'), initial='00500')


class IssueListView(DefaultListView):
    model = Issue


class IssueCreateView(CreateView):
    model = Issue
    template_name = 'core/form.html'
    success_url = '/manage/issues/'


class IssueEditView(DefaultEditView):
    model = Issue


def welcome(request):
    form = WarrantyForm()
    return render(request, "issues/welcome.html", locals())


def details(request, device=None):
    form = IssueForm()

    if request.method == "POST":
        request.session['details'] = request.POST.get('details')
        return redirect(list_issues)

    return render(request, "issues/details.html", locals())


def list_issues(request, idx=None):
    questions = Question.objects.filter(issue__sp_id=1)

    if idx is None:
        question = questions[0]
        request.session['choices'] = []
    else:
        choice = Choice.objects.get(pk=idx)
        current = questions.get(choice=idx)
        current.choice = choice
        request.session['choices'].append(current)
        try:
            question = current.next()
        except Question.DoesNotExist:
            return redirect(customer)

    dump = request.session.get('choices')
    return render(request, "issues/question.html", locals())


def choose_device(request, device=None):
    models = products.models()
    tpl = "issues/%s.html" % (device or 'device')
    return render(request, tpl, locals())


def warranty(request):
    return render(request, "issues/warranty.html", locals())


def thanks(request):
    return render(request, "issues/thanks.html", locals())


def customer(request):
    form = CustomerForm()
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            return redirect(thanks)

    return render(request, "issues/customer.html", locals())


def restart(request):
    request.session = None
    return redirect(welcome)
