from django import forms
from apps.issues.models import Issue


class IssueForm(forms.ModelForm):
    class Meta:
        model = Issue
