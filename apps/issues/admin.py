from django.contrib import admin
from apps.issues.models import Issue, Question, Choice

admin.site.register(Issue)
admin.site.register(Question)
admin.site.register(Choice)
